<!-- Home static blocks -->
		<?php

		// vars
		$static_block_left_title = get_field('static_block_left_title');
		$static_block_left_description = get_field('static_block_left_description');
		$static_block_left_image = get_field('static_block_left_image');
		$static_block_left_button_text = get_field('static_block_left_button_text');
		$static_block_left_button_url = get_field('static_block_left_button_url');

		$static_block_right_title = get_field('static_block_right_title');
		$static_block_right_description = get_field('static_block_right_description');
		$static_block_right_image = get_field('static_block_right_image');
		$static_block_right_button_text = get_field('static_block_right_button_text');
		$static_block_right_button_url = get_field('static_block_right_button_url');

		?>
        <section class="main-content container">
            <div class="main-content-left">
                <div class="image-left">
                    <img src="<?php echo $static_block_left_image['url']; ?>" alt="<?php echo $static_block_left_image['alt'] ?>" />
                </div>
                <div class="post-info">
                    <h2 class="link"><a href="<?php echo $static_block_left_button_url; ?>"><?php echo $static_block_left_title; ?></a></h2>
                    <p><?php echo $static_block_left_description; ?></p>
                    <a class="learn" href="<?php echo $static_block_left_button_url; ?>"><?php echo $static_block_left_button_text; ?></a>
                </div>
            </div>
            <div class="main-content-right">
                <div class="image-right">
                    <img src="<?php echo $static_block_right_image['url']; ?>" alt="<?php echo $static_block_right_image['alt'] ?>" />
                </div>
                <div class="post-info">
                    <h2 class="link"><a href="<?php echo $static_block_right_button_url; ?>"><?php echo $static_block_right_title; ?></a></h2>
                    <p><?php echo $static_block_right_description; ?></p>
                    <a class="learn" href="<?php echo $static_block_right_button_url; ?>"><?php echo $static_block_right_button_text; ?></a>
                </div>
            </div>
        </section>

<!-- Company features -->
<?php if( have_rows('company_features') ): ?>

            <section class="footer-top container">

                <?php while( have_rows('company_features') ): the_row();

                    // vars
                    $feature_text = get_sub_field('feature_text');
                    $features_icon = get_sub_field('features_icon');

                    ?>

                    <div class="featured">
                        <span aria-hidden="true" class="footer-top-icon <?php echo $features_icon; ?>"></span>
                        <a><?php echo $feature_text; ?></a>
                    </div>

                <?php endwhile; ?>

            </section>

        <?php endif; ?>

