<?php if ( have_rows( 'services_flexible_content' ) ): ?>
	<?php $i = 1; ?>
	<?php while ( have_rows( 'services_flexible_content' ) ) : the_row(); ?>
		<!-- if -->
		<?php if ( get_row_layout() == 'text_section_with_columns' ) : ?>
			<?php if ( get_sub_field( 'text_section_with_columns_text_background_picture' ) ) : ?>
				<section class="three-column-text order-<?php echo $i; ?>" style="background-image: url('<?php the_sub_field("text_section_with_columns_text_background_picture"); ?>')">
				<?php else : ?>
				<section class="three-column-text order-<?php echo $i; ?>">
				<?php endif; ?>
				  <div class="container">
				    <?php the_sub_field('text_section_with_columns_header'); ?>
				    <style>
				    <?php if ( get_sub_field( 'text_section_with_columns_text_background_picture' ) ) : ?>
				      .three-column-text.order-<?php echo $i; ?> h2,
				      .three-column-text.order-<?php echo $i; ?> .columns p {
				        color: #fff;
				      }
				      .three-column-text {
				        background-size: cover;
				        background-position: center;
				      }
				      @media (min-width: 1180px) {
					      .three-column-text.order-<?php echo $i; ?> {
					      	padding-bottom: 400px;
					      }
				      }
				      <?php endif; ?>
				      .three-column-text {
				        background-color: <?php the_sub_field('text_section_with_columns_text_background_color'); ?>
				      }
				      @media (min-width: 1180px) {
				        .three-column-text .columns {
				          column-count: <?php if(get_sub_field('text_section_with_columns_col')) { the_sub_field('text_section_with_columns_col'); } else {echo '3';} ?>;
				        }
				      }
				    </style>
				    <div class="columns">
				      <?php the_sub_field('text_section_with_columns_text'); ?>
				    </div>
				  </div>
				</section>
		<!-- else if -->
		<?php elseif ( get_row_layout() == 'text_with_list' ) : ?>
			<section class="text-with-list">
			  <div class="container">
			    <?php the_sub_field('text_with_list_title'); ?>
			    <ul>
			      <?php
			        if( have_rows('text_with_list_list') ):
			          while ( have_rows('text_with_list_list') ) : the_row(); ?>
			            <li><?php the_sub_field('list_item'); ?></li>
			          <?php
			          endwhile;
			        else :
			        endif;
			      ?>
			    </ul>
			  </div>
			</section>
		<!-- else if -->
		<?php elseif ( get_row_layout() == 'simple_image' ) : ?>
			<?php if ( get_sub_field( 'display_just_image' ) ) { ?>
				<img src="<?php the_sub_field( 'display_just_image' ); ?>" />
			<?php } ?>
		<!-- else if -->
		<?php elseif ( get_row_layout() == 'company_services_blocks' ) : ?>
		<?php if ( have_rows( 'company_services' ) ) : ?>
			<section class="footer-top container">
			<?php while ( have_rows( 'company_services' ) ) : the_row();
				// vars
                $feature_text = get_sub_field('feature_text');
                $features_icon = get_sub_field('features_icon');
                $feature_url = get_sub_field('feature_url');
			?>
				<div class="featured">
	                <span aria-hidden="true" class="footer-top-icon <?php echo $features_icon; ?>"></span>
	                <?php echo $feature_text; ?>
	                <a href="<?php echo $feature_url; ?>">Learn more</a>
	            </div>
			<?php endwhile; ?>
		</section>
		<?php else : ?>
			<?php // no rows found ?>
		<?php endif; ?>
		<!-- start endif -->
		<?php endif; ?>
    <?php $i++ ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>