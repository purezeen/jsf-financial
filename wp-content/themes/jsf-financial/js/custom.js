( function( $ ) {

    $(document).ready(function(){

        $(".site-header").on('click', '.menu-toggle', function(e) {
            // e.preventDefault();
            e.stopPropagation();
            $(".main-navigation").toggleClass('open');
            $(".site-header").toggleClass('open');
        });

        if($(window).width() < 1024 ) {
          $(".menu-item-has-children > a").click(function(e){
            // block parent link in menu, and trigger submenu
            e.preventDefault();
            $(this).parent().toggleClass('opened');
          });
        }


        //Program created by Ryan Tarson Updated 6.15.16, under this code is my pure JS Version
        var wHeight = window.innerHeight;
        //search bar middle alignment
        $('#mk-fullscreen-searchform').css('top', wHeight / 2.5);
        //reform search bar
        jQuery(window).resize(function() {
            wHeight = window.innerHeight;
            $('#mk-fullscreen-searchform').css('top', wHeight / 2.5);

            if($(window).width() < 1024 ) {
              $(".menu-item-has-children > a").click(function(e){
                // block parent link in menu, and trigger submenu
                e.preventDefault();
                $(this).parent().toggleClass('opened');
              });
            } else {
              $(".menu-item-has-children > a").click(function(e){
                // block parent link in menu, only submenu links work
                e.preventDefault();
              });
            }

        });
        // Search
        $('#search-button').click(function() {
            
            $("div.mk-fullscreen-search-overlay").addClass("mk-fullscreen-search-overlay-show");
        });
        $("a.mk-fullscreen-close").click(function() {
            
            $("div.mk-fullscreen-search-overlay").removeClass("mk-fullscreen-search-overlay-show");
        });

        // init flex slider
        $('.flexslider').flexslider({
            animation: 'slide',
            controlsContainer: '.flex-container',
            directionNav: false,
            animationLoop: false
        });

    });

} )( jQuery );
