<?php
/* Template Name: Resource Center Repository page */

get_header();
?>

<section class="resource-content">
    
    <div class="main-post-content container">

        <div class="posts">
            <h2 class="title">What's trending</h2>
			<?php $the_custom_post_query = new WP_Query( array( 'post_type' => 'resource-centers', 'order'   => 'ASC', 'posts_per_page' => 3 ) ); ?>
			<?php if ( $the_custom_post_query->have_posts() ) : ?>

				<?php while ($the_custom_post_query -> have_posts()) : $the_custom_post_query -> the_post(); ?>

                    <!-- article -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class('images'); ?>>
                        <div class="img-l">

							<?php the_post_thumbnail('archive-size'); // Fullsize image for the single post ?>
                            <p><span><?php the_time('j'); ?></span> <?php the_time('F'); ?></p>
                        </div>
                        <div class="img-r">
                            <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ?><?php the_title(); ?></a></h2>
                            <p class="author">By <span class="by"><?php the_author_posts_link(); ?></span> in <span class="by"><?php echo get_modified_term_list( get_the_ID(), 'resource_category', '', ', ', '', array(68) ); ?></span></p>
                            <span class="short-desc"><?php the_content(''); ?></span>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                        </div>
                    </div>
                    <!-- /article -->

				<?php endwhile;

				wp_reset_postdata();

			else: ?>

                <!-- article -->
                <div>
                    <h2><?php _e( 'Sorry, there is no service at the moment.', 'html5blank' ); ?></h2>
                </div>
                <!-- /article -->

			<?php endif; ?>
        </div>
        <div class="posts-right">
			<?php get_sidebar(); ?>
        </div>
    </div>
    <section class="resource-center-highlights container">
        <div class="title">
            <p>Resource center highlights</p>
        </div>
        <div class="highlighted-resources">
            <?php  
            $custom_post_args = array(
                'post_type' => 'resource-centers',
                'order'   => 'ASC',
                'posts_per_page' => 3,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'resource_category',
                        'field'    => 'slug',
                        'terms'    => 'highlights',
                    ),
                ),
            );
            ?>
            <?php $the_custom_post_query = new WP_Query( $custom_post_args ); ?>
            <?php if ( $the_custom_post_query->have_posts() ) : ?>
                <?php while ($the_custom_post_query -> have_posts()) : $the_custom_post_query -> the_post(); ?>

                    <!-- article -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class('center-highlight'); ?>>
                        <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                        <p><?php the_title(); ?></p>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                    </div>
                    <!-- /article -->

                <?php endwhile;

                wp_reset_postdata();

            else: ?>

                <!-- article -->
                <div>
                    <h2><?php _e( 'Sorry, there is no service at the moment.', 'html5blank' ); ?></h2>
                </div>
                <!-- /article -->

            <?php endif; ?>

        </div>
    </section>

</section>
<?php get_footer(); ?>

