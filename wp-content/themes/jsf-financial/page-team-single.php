<?php /* Template Name: Single Team Page */ ?>
<?php get_header(); ?>

    <section class="single-team-member">
        <div class="highlighted">
            <div class="wrapper">
                <p>Our team - Investment management</p>
            </div>
        </div>
        <div class="member-wrapper">
            <div class="member-info">
                <h1>Jeff Fishman</h1>
                <p class="title">Founder &amp; Managing Member / Investment Advisor Representative</p>
                <p class="info"> Ut suscipit diam faucibus, efficitur odio nec, pulvinar lacus. Proin purus nisi, maximus et ante eu, rhoncus vehicula felis. Integer nec augue risus. Mauris sodales est nec lectus viverra, a consequat dolor elementum. Ut ac enim non libero mollis rhoncus vel et erat. Integer ornare blandit lobortis. Aenean vitae posuere mauris, ac vestibulum diam. Fusce faucibus lorem et sem mattis, in mollis erat semper. Sed eget augue eget odio pellentesque vehicula a a ex. Quisque condimentum eros nec eros tempor tristique. Ut eget ornare urna. Praesent condimentum felis id urna tincidunt ullamcorper. Praesent ac magna ultrices, congue enim quis, rhoncus est. Maecenas maximus arcu ullamcorper, interdum purus et, hendrerit orci. Etiam lacinia risus nec viverra laoreet.
                    Nulla leo elit, bibendum nec erat ac, volutpat mattis quam. Proin placerat, lacus sed semper hendrerit, massa justo varius augue, ut sagittis felis dolor scelerisque nunc. Donec tristique consectetur imperdiet. Nam vulputate augue in nunc aliquam, sit amet euismod enim consequat. Pellentesque imperdiet condimentum varius. Aenean id justo in neque mollis vestibulum. Donec tempor, dui ac venenatis sodales, ante felis lacinia ante, at efficitur elit sapien viverra eros. Fusce tempus eros non libero euismod, quis mattis quam posuere. Curabitur id luctus eros. Praesent euismod porta risus, vitae semper enim luctus ac. Nullam nec risus a ante fringilla placerat vel quis diam. In ut placerat dolor. Vestibulum purus sem, commodo non fringilla sit amet, efficitur sit amet nibh. Mauris sit amet fringilla mi, sit amet congue odio. Integer sed varius ex, id tincidunt nisl.
                    Nam molestie ex id massa hendrerit gravida. Duis eu fringilla nisl, vel elementum augue. Praesent tortor lorem, malesuada sit amet posuere at, egestas vitae enim. Duis lacinia lorem mattis, maximus neque sit amet, feugiat lacus. Donec finibus a lectus eu tincidunt. Phasellus sed neque posuere, egestas nulla nec, facilisis ipsum. Duis vitae nulla enim. Vestibulum a ornare diam. Cras auctor lectus dolor, id rhoncus dolor cursus et. In ullamcorper pretium rhoncus. Cras lobortis risus ex, eget dapibus est egestas vel. Sed congue, lacus tempor finibus tempus, elit dui volutpat mauris, at mollis orci elit non urna. Suspendisse et enim quam. Quisque ultrices metus ac consectetur luctus. Etiam id massa dignissim, tristique lorem et, posuere justo. Phasellus feugiat scelerisque dolor ac finibus.
                </p>
                <a href="/team-2">Back to team page</a>
            </div>
            <div class="member-photo">
                <img src="<?php bloginfo('template_directory'); ?>/img/Jeff-Fishman-JSF-e1366915309707-280x350.jpg" alt="member photo">
            </div>
        </div>
    </section>

<?php get_footer(); ?>