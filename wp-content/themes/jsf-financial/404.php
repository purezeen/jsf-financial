<?php get_header(); ?>
<div class="hero-wrapper">
    <div class="flex-container">
        <div class="flexslider">
            <div class="container">
                <ul class="slides">
                    <li>
                        <div class="container">
                            <div class="flex-caption">
                            	<h2>
                              <?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'jsf-financial' ); ?>
                            </h2>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
<section class="resource-content">
    <div class="main-post-content container">

        <div class="posts">
            <h2 class="title">Page not found</h2>
            <div class="images">
	            <div class="img-r">
	                <h2><?php esc_html_e( 'It looks like nothing was found at this location.', 'jsf-financial' ); ?></h2>
	                	<p></p>
	                	<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
	            </div>
            </div>
        </div>
        <div class="posts-right">
            <?php get_sidebar(); ?>
        </div>
    </div>

</section>
<?php get_footer(); ?>

