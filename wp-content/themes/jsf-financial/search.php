<?php get_header(); ?>
<div class="hero-wrapper">
    <div class="flex-container">
        <div class="flexslider">
            <div class="container">
                <ul class="slides">
                    <li>
                        <div class="container">
                            <div class="flex-caption">
                            	<h2>
                              <?php echo sprintf( __( '%s Search Results for ', 'jsf-financial' ), $wp_query->found_posts ); echo get_search_query(); ?>
                            </h2>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
<section class="resource-content">
    <div class="main-post-content container">

        <div class="posts">
            <h2 class="title">Search results</h2>
            <?php while ( have_posts() ) : the_post(); ?>

                <!-- article -->
                <div id="post-<?php the_ID(); ?>" <?php post_class('images'); ?>>
                    <div class="img-r">
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>

                        <?php $content = get_the_content(''); ?>
                        <span class="short-desc"><p><?php echo substr($content, 0, 220);?>...</p></span>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                    </div>
                </div>
                <!-- /article -->

                <?php endwhile; ?>
                
                <!-- pagination -->
                <div class="pagination">
                    <?php jsf_financial_wp_pagination(); ?>
                </div>
                <!-- /pagination -->
        </div>
        <div class="posts-right">
            <?php get_sidebar(); ?>
        </div>
    </div>

</section>
<?php get_footer(); ?>

