<?php if( get_field('select_hero_type') ):

		$post_object = get_field('select_post');

			// override $post
			$post = $post_object;
			setup_postdata( $post );

			?>
			<div class="hero-wrapper">
			    <div class="hero overlay" style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center
                        center; background-size: cover;">
			        <div class="container">
                        <div class="caption">
                            <h1><?php the_title(); ?></h1>
                            <h5 class="by">By <span class="author"><?php the_author_posts_link(); ?></span> on <span class="date"><?php the_time('M d'); ?></span> in <span class="tag"><?php echo get_modified_term_list( get_the_ID(), 'resource_category', '', ', ', '', array(68) ); ?><?php exclude_post_categories('12', ', '); ?></span></h5>
	                        <?php $content = get_the_content(''); ?>
                            <h4><?php echo substr($content, 0, 220);?>...</h4>
                            <span class="hero-link"><a href="<?php the_permalink(); ?>">Read more</a></span>
                        </div>
                    </div>
			    </div>
			</div>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php else: ?> 

	<?php if( have_rows('flexslider-repeater') ): ?>
	    <div class="hero-wrapper">
	        <div class="flex-container">
	            <div class="flexslider">
	                <div class="container">
                        <ul class="slides">

					                <?php while( have_rows('flexslider-repeater') ): the_row();

						                // vars
						                $flex_image = get_sub_field('flex_image');
						                $flex_caption = get_sub_field('flex_caption');
						                $flex_description = get_sub_field('flex_description');
						                $flex_button = get_sub_field('flex_button');
						                $flex_url = get_sub_field('flex_url');
						                $flex_color = get_sub_field('flex_color');
						                $flex_opacity = get_sub_field('flex_opacity');

						                ?>

														<?php if( get_sub_field('flex_display_image_like_background') ): ?>

                            <li style="background-image: url('<?php echo $flex_image['url']; ?>'); background-position: center;" class="<?php echo $flex_opacity ?> overlay">

                          	<?php else: ?>

                            <li style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/SVG/logo-lines-new.svg'); background-position: top right; background-color: <?php echo $flex_color ?>">

                          	<?php endif; ?>

                              <div class="container">
                                <div class="flex-caption">

									                <?php if( $flex_caption ): ?>
                                  <h2><?php echo $flex_caption; ?></h2>
									                <?php endif; ?>

										                <?php if( $flex_description ): ?>
                                			<h4><?php echo $flex_description; ?></h4>
										                <?php endif; ?>

										                <?php if( $flex_button ): ?>
                                      <div class="hero-link"><a href="<?php echo $flex_url; ?>"><?php echo $flex_button; ?></a></div>
										                <?php endif; ?>

                                </div>

															<?php if( get_sub_field('flex_display_image_like_background') == 0 ): ?>
                                <div class="flex-image-border">
                                  <div class="flex-image">
                                    <img src="<?php echo $flex_image['url']; ?>" alt="<?php echo $flex_image['alt'] ?>" />
                                  </div>
                                </div>
                              <?php endif; ?>
                              </div>
                            </li>

					                <?php endwhile; ?>

                        </ul>
                    </div>
	            </div>
	        </div>
	    </div>
	<?php endif; ?>

<?php endif; ?>
