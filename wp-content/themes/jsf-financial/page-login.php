<?php
/* Template Name: Login */

get_header();
?>
<section class="three-column-text login-page">
  <div class="container">
    <h2><?php echo esc_html( get_the_title() ); ?></h2>
    <div class="clearfix">
      <div class="contact-wrap">
        <?php if( have_rows('login_links') ): ?>

          <ul class="logins">

          <?php while( have_rows('login_links') ): the_row(); 

            // vars
            $login_heading = get_sub_field('login_heading');
            $login_url = get_sub_field('login_url');
            $additional_description_checker = get_sub_field('additional_description_checker');
            $additional_description = get_sub_field('additional_description');
            ?>

            <li>
                <h3><?php echo $login_heading; ?></h3>
                <a href="<?php echo $login_url; ?>" target="_blank">Log in</a>
                <?php if( $additional_description_checker ): ?>
  
                  <?php echo $additional_description; ?>
                  
                <?php endif; ?>

            </li>

          <?php endwhile; ?>

          </ul>

        <?php endif; ?>
      </div>
      <div class="additional-information">
        <?php
        if ( have_posts() ) : while ( have_posts() ) : the_post();
          the_content();
        endwhile; endif; 
        ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
