<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jsf-financial
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<?php
if ( is_page(22) || has_term( '', 'resource_category' ) ):
$args = array(
   'taxonomy' => 'resource_category',
   'exclude' => '68'
);
$argsTag = array(
   'taxonomy' => 'resource_tag'
);
	else:
$args = array(
   'taxonomy' => 'category',
   'exclude' => '12'
);
$argsTag = array(
   'taxonomy' => 'post_tag'
);
endif;
?>
<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>

	<section class="widget widget_categories">
		<h2 class="widget-title">Categories</h2>
		<ul>
	  <?php
	  $categories = get_categories( $args );
	  foreach ( $categories as $category ) {
	     echo '<li><a href="' . get_category_link( $category->term_id ) . '" rel="bookmark">' . $category->name . '' . $category->description . '</a></li>';
	  }
     ?>
		</ul>
	</section>
	<section class="widget widget_tag_cloud">
		<h2 class="widget-title">Tags</h2>
		<div class="tagcloud">
		<?php
	  $terms = get_terms( $argsTag );
	  foreach ( $terms as $term ) {
	     echo '<a href="' . get_term_link( $term ) . '" rel="tag" class="tag-cloud-link">' . $term->name . '' . $term->description . '</a>';
	  }
	  ?>
		</div>
	</section>
	<?php dynamic_sidebar( 'sidebar-2' ); ?>
</aside><!-- #secondary -->
