<?php /* Template Name: Team Page */ ?>
<?php get_header(); ?>
    <!-- Start Repeater -->
<?php if( have_rows('team_repeater')): // check for repeater fields ?>

    <section class="team-members">


		<?php while ( have_rows('team_repeater')) : the_row(); // loop through the repeater fields ?>

			<?php // set up post object and vars
			$team_add_sector = get_sub_field('team_add_sector');
			$team_sector_title = get_sub_field('team_sector_title');
			$team_sector_description = get_sub_field('team_sector_description');
			?>

			<?php if( $team_add_sector): ?>

                <div class="gray-box">
                    <div class="inner">
                        <h2><?php echo $team_sector_title ?></h2>
                        <p><?php echo $team_sector_description ?></p>
                    </div>
                </div>

			<?php endif; ?>



            <div class="team-management container">
                    <?php if( have_rows('add_new_team_member')): // check for repeater fields ?>

                    <?php while ( have_rows('add_new_team_member')) : the_row(); // loop through the repeater?>

                    <?php //foreach start here if needed ?>
                    <?php 
                        $post_object = get_sub_field('select_team_in_this_section');
                        $post = $post_object; //only for single selection for multiple use foreach
                        setup_postdata( $post );
                    ?>

                    <div class="team-member">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <div class="member-image-border">
                                <div class="member-image">
		                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                                </div>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <p>
								<?php

                                $terms = get_the_terms( $post->ID , 'teams_positions');
                                // init counter
                                $i = 1;
                                foreach( $terms as $term):
                                    $term_link = get_term_link( $term, 'teams_positions' );
                                        if( is_wp_error( $term_link ) )
                                        continue;
                                        echo $term->name;
                                        //  Add comma (except after the last theme)
                                        echo ($i < count($terms))? " / " : "";
                                        // Increment counter
                                        $i++;
                                endforeach;
                                    
                                ?>
                            </p>
                        </a>
                    </div>
                    <?php //foreach end here if needed ?>

                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endwhile; ?>
                    <!-- End Repeater -->
                    <?php endif; ?>
				

            </div>

			


		<?php endwhile; ?>

    </section>
    <!-- End Repeater -->
<?php endif; ?>

<?php get_footer(); ?>