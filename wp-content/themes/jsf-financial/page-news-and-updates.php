<?php
/* Template Name: News & Updates page */

get_header();
?>

<section class="resource-content">
    
    <div class="main-post-content container">

        <div class="posts">
            <h2 class="title">Recent Updates</h2>
						<?php $the_query = new WP_Query( 'posts_per_page=3' ); ?>
						 
						<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

              <!-- article -->
              <div id="post-<?php the_ID(); ?>" <?php post_class('images'); ?>>
                  <div class="img-l">
                      <?php the_post_thumbnail('archive-size'); // Fullsize image for the single post ?>
                      <p><span><?php the_time('d'); ?></span> <?php the_time('F'); ?></p>
                  </div>
                  <div class="img-r">
                      <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                      <p class="author">By <span class="by"><?php the_author_posts_link(); ?></span> in <span class="by"><?php exclude_post_categories('12', ', '); ?></span></p>

                      <span class="short-desc"><?php echo the_content(''); ?></span>
                      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                  </div>
              </div>
              <!-- /article -->
							<?php 
							endwhile;
							wp_reset_postdata();
							?>
        </div>
        <div class="posts-right">
            <?php get_sidebar(); ?>
        </div>
    </div>
    <section class="resource-center-highlights container">
        <div class="title">
            <p>What's trending</p>
        </div>
        <div class="highlighted-resources">
    			<?php $the_custom_post_query = new WP_Query( array( 'post_type' => 'post', 'order'   => 'ASC', 'posts_per_page' => 3, 'category_name' => 'highlights' ) ); ?>
    			<?php if ( $the_custom_post_query->have_posts() ) : ?>
    				<?php while ($the_custom_post_query -> have_posts()) : $the_custom_post_query -> the_post(); ?>

                        <!-- article -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('center-highlight'); ?>>
    						<?php the_post_thumbnail(); // Fullsize image for the single post ?>
                            <p><?php the_title(); ?></p>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                        </div>
                        <!-- /article -->

    				<?php endwhile;

    				wp_reset_postdata();

    			else: ?>

                    <!-- article -->
                    <div>
                        <h2><?php _e( 'Sorry, there is no service at the moment.', 'html5blank' ); ?></h2>
                    </div>
                    <!-- /article -->

    			<?php endif; ?>

        </div>
    </section>

</section>
<?php get_footer(); ?>

