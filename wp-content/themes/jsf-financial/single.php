<?php get_header(); ?>

<section class="resource-content">
    <div class="hero-post">
        <img src="<?php the_post_thumbnail_url(); ?>" alt="Hero image">
    </div>
    <div class="main-post-content content-indent container">
        <div id="post-<?php the_ID(); ?>" <?php post_class('post-article'); ?>>
            <h1><?php the_title(); ?></h1>
            <div class="description">
				<?php while ( have_posts() ) : the_post();?>
                    <p class="authored-by">By <span><?php the_author_posts_link(); ?></span> on <?php the_time('M d'); ?> in <span><?php exclude_post_categories('12', ', '); ?></span>
                    </p>

                    <span class="text">
				    <?php the_content(); ?>
                </span>

				<?php endwhile; // End of the loop. ?>
            </div>

            <div class="pager clearfix">
                <?php previous_post_link('%link', 'PREV'); ?> 
                <?php next_post_link('%link', 'NEXT'); ?> 
            </div>

        </div>

        <div class="posts-right">
           <!--  <div class="about-author">
                <div class="short-info">
                    <p class="post-date"><span><?php the_time('d'); ?> </span><?php the_time('F'); ?></p>
                    <div class="author-img">
                        <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
                    </div>
                </div>
                <div class="author-social">
                    <?php 

                    // vars
                    $user_facebook = get_the_author_meta('user_facebook');
                    $user_twitter = get_the_author_meta('user_twitter');
                    $user_linkedin = get_the_author_meta('user_linkedin');
                    $user_pinterest = get_the_author_meta('user_pinterest');
                    $user_google_plus = get_the_author_meta('user_google_plus');
                     ?>
                    <?php if ( $user_facebook ): ?>                        
                    <a href="<?php echo $user_facebook ?>" target="_blank"><span class="social_facebook_square" aria-hidden="true""></span></a>
                    <?php endif ?>

                    <?php if ( $user_twitter ): ?>  
                    <a href="<?php echo $user_twitter ?>" target="_blank"><span class="social_twitter_square" aria-hidden="true""></span></a>
                    <?php endif ?>

                    <?php if ( $user_linkedin ): ?>  
                    <a href="<?php echo $user_linkedin ?>" target="_blank"><span class="social_linkedin_square" aria-hidden="true""></span></a>
                    <?php endif ?>

                    <?php if ( $user_pinterest ): ?>  
                    <a href="<?php echo $user_pinterest ?>" target="_blank"><span class="social_pinterest_square" aria-hidden="true""></span></a>
                    <?php endif ?>

                    <?php if ( $user_google_plus ): ?>  
                    <a href="<?php echo $user_google_plus ?>" target="_blank"><span class="social_googleplus_square " aria-hidden="true""></span></a>
                    <?php endif ?>
                </div>
                <div class="author-info">
                    <p class="about">About the author</p>
                    <p class="name"><a href="<?php echo get_the_author_meta('url'); ?>"><?php echo get_the_author_meta('first_name'); ?> <?php echo get_the_author_meta('last_name'); ?></a></p>
                    <p class="desc"><?php echo get_the_author_meta('description'); ?></p>
                </div>
            </div> -->
			<?php get_sidebar(); ?>
        </div>
    </div>
    <div class="resource-center-highlights container">
        <div class="title">
            <p>What's trending</p>
            <a href="<?php echo get_permalink( get_page_by_path( 'news updates' ) ); ?>">view all</a>
        </div>
        <div class="highlighted-resources">
                <?php $the_custom_post_query = new WP_Query( array( 'post_type' => 'post', 'order'   => 'ASC', 'posts_per_page' => 3, 'category_name' => 'highlights' ) ); ?>
                <?php if ( $the_custom_post_query->have_posts() ) : ?>
                    <?php while ($the_custom_post_query -> have_posts()) : $the_custom_post_query -> the_post(); ?>

                        <!-- article -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('center-highlight'); ?>>
                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                            <p><?php the_title(); ?></p>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                        </div>
                        <!-- /article -->

                    <?php endwhile;

                    wp_reset_postdata();

                else: ?>

                    <!-- article -->
                    <div>
                        <h2><?php _e( 'Sorry, there is no service at the moment.', 'html5blank' ); ?></h2>
                    </div>
                    <!-- /article -->

                <?php endif; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>

