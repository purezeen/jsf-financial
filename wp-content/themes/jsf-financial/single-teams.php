<?php get_header(); ?>

    <section class="single-team-member">
        <div class="highlighted">
            <div class="wrapper container">
                <p>Our team</p>
            </div>
        </div>
        <div class="member-wrapper container">
            <div class="member-info">
                <h1><?php the_title(); ?></h1>
                <p class="title">
                    <?php

                    $terms = get_the_terms( $post->ID , array( 'teams_positions') );
                    // init counter
                    $i = 1;
                    foreach ( $terms as $term ) {
                        $term_link = get_term_link( $term, array( 'teams_positions') );
                            if( is_wp_error( $term_link ) )
                            continue;
                            echo $term->name;
                            //  Add comma (except after the last theme)
                            echo ($i < count($terms))? " / " : "";
                            // Increment counter
                            $i++;
                    }
                        
                    ?>
                </p>
                <?php while ( have_posts() ) : the_post(); ?>
                    <span class="info"><?php echo the_content(); ?></span>
                <?php endwhile; ?>
                
                <a href="/team">Back to team page</a>
            </div>
            <div class="member-photo">

               <?php 
                    if( has_post_thumbnail() ) {
                        the_post_thumbnail('team_person');
                        $no_image = '';
                    } else {
                        $no_image = 'no-image';
                    }
               ?>
               
               <div class="info <?php echo $no_image ?>">
                <?php if( get_field('team_contact_mail') ): ?>
                    <p class="mail">
                        <?php the_field('team_contact_mail') ?>
                    </p>
                <?php endif; ?>

                <?php if( get_field('team_contact_phone') ): ?>
                    <p class="phone">
                        <?php the_field('team_contact_phone') ?>
                    </p>
                <?php endif; ?>
               </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>