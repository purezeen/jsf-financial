<?php /* Template Name: Front Page */ ?>
<?php get_header(); ?>
<?php the_content(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        
        <?php if( have_rows('company_features') ): ?>

            <section class="footer-top container">

                <?php while( have_rows('company_features') ): the_row();

                    // vars
                    $feature_text = get_sub_field('feature_text');
                    $features_icon = get_sub_field('features_icon');
                    $feature_url = get_sub_field('feature_url');
                    ?>

                    <div class="featured">
                        <span aria-hidden="true" class="footer-top-icon <?php echo $features_icon; ?>"></span>
                        <?php echo $feature_text; ?>
                        <a href="<?php echo $feature_url; ?>">Learn more</a>
                    </div>

                <?php endwhile; ?>

            </section>

        <?php endif; ?>

        <section class="resource-center-highlights container">
            <div class="title">
                <p>Resource center highlights</p>
                <a href="<?php echo get_permalink( get_page_by_path( 'resource center' ) ); ?>">view all</a>
            </div>
            <div class="highlighted-resources">
                <?php  
                $custom_post_args = array(
                    'post_type' => 'resource-centers',
                    'order'   => 'ASC',
                    'posts_per_page' => 3,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'resource_category',
                            'field'    => 'slug',
                            'terms'    => 'highlights',
                        ),
                    ),
                );
                ?>
                <?php $the_custom_post_query = new WP_Query( $custom_post_args ); ?>
                <?php if ( $the_custom_post_query->have_posts() ) : ?>
                    <?php while ($the_custom_post_query -> have_posts()) : $the_custom_post_query -> the_post(); ?>

                        <!-- article -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('center-highlight'); ?>>
                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                            <p><?php the_title(); ?></p>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                        </div>
                        <!-- /article -->

                    <?php endwhile;

                    wp_reset_postdata();

                else: ?>

                    <!-- article -->
                    <div>
                        <h2><?php _e( 'Sorry, there is no service at the moment.', 'html5blank' ); ?></h2>
                    </div>
                    <!-- /article -->

                <?php endif; ?>

            </div>
        </section>

		<?php $the_query = new WP_Query(  array( 'category_name' => 'front-page-promotion' ) ); ?>

		<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

            <section class="hero-bottom clearfix" style="background: url('<?php the_post_thumbnail_url(); ?>')
                    no-repeat center center;
                    background-size: cover;">
                <!-- article -->
                <div class="container" >
                    <div  id="post-<?php the_ID(); ?>" <?php post_class( 'hero-bottom-caption' ); ?>>

                        <!-- post title -->
                        <h2>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                        </h2>
                        <!-- /post title -->

                        <!-- post details -->
                        <span class="date"><?php the_time('j F Y'); ?></span>
                        <!-- /post details -->

                        <span class="caption"><?php echo the_content(''); ?></span>

                        <div class="link">
                            <a href="<?php echo get_permalink(); ?>" class="default-button">Read more</a>
                        </div>

                        <!-- /article -->
                    </div>
                </div>
            </section>

			<?php
		endwhile;
		wp_reset_postdata();
		?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer(); ?>
