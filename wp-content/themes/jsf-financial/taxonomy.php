<?php get_header(); ?>
<div class="hero-wrapper">
    <div class="flex-container">
        <div class="flexslider">
            <div class="container">
                <ul class="slides">
                    <li style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/SVG/logo-lines-new.svg'); background-position: top right; background-color: rgb(51, 100, 151)">
                        <div class="container">
                            <div class="flex-caption">
                                <?php
								the_archive_title( '<h2 class="page-title">', '</h2>' );
								?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<section class="resource-content">
    <div class="main-post-content container">
        <div class="posts">
            <h2 class="title">Display results</h2>
            <?php while ( have_posts() ) : the_post(); ?>

                                <!-- article -->
                <div id="post-<?php the_ID(); ?>" <?php post_class('images'); ?>>
                    <div class="img-l">
                        <?php the_post_thumbnail('archive-size'); // Fullsize image for the single post ?>
                        <p><span><?php the_time('j'); ?></span> <?php the_time('F'); ?></p>
                    </div>
                    <div class="img-r">
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                        <p class="author">By <span class="by"><?php the_author_posts_link(); ?></span> in <span class="by"><?php echo get_modified_term_list( get_the_ID(), 'resource_category', '', ', ', '', array(68) ); ?></span></p>

                        <span class="short-desc"><?php echo the_content(''); ?></span>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read more</a>
                    </div>
                </div>
                <!-- /article -->

                <?php endwhile; ?>
                <!-- pagination -->
                <div class="pagination">
                    <?php jsf_financial_wp_pagination(); ?>
                </div>
                <!-- /pagination -->
        </div>
        <div class="posts-right">
            <?php get_sidebar(); ?>
        </div>
    </div>

</section>
<?php get_footer(); ?>

