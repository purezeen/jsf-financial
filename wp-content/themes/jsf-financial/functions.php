<?php
/**
 * jsf-financial functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package jsf-financial
 */

if ( ! function_exists( 'jsf_financial_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jsf_financial_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on jsf-financial, use a find and replace
		 * to change 'jsf-financial' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jsf-financial', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-top' => esc_html__( 'Main nav top', 'jsf-financial' ),
			'menu-bottom' => esc_html__( 'Main nav bottom', 'jsf-financial' ),
			'footer-1' => esc_html__( 'Footer 1', 'jsf-financial' ),
			'footer-2' => esc_html__( 'Footer 2', 'jsf-financial' ),
			'footer-3' => esc_html__( 'Footer 3', 'jsf-financial' ),
			'footer-4' => esc_html__( 'Footer 4', 'jsf-financial' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jsf_financial_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    // add_image_size('large', 700, '', true); // Large Thumbnail
    // add_image_size('medium', 250, '', true); // Medium Thumbnail
    // add_image_size('small', 120, '', true); // Small Thumbnail
    // add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('team_person', 400, 500, true);
    add_image_size('archive-size', 360, '', true);
	}
endif;
add_action( 'after_setup_theme', 'jsf_financial_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jsf_financial_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'jsf_financial_content_width', 640 );
}
add_action( 'after_setup_theme', 'jsf_financial_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jsf_financial_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jsf-financial' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jsf-financial' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jsf-financial' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'jsf-financial' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'jsf_financial_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jsf_financial_scripts() {

	wp_enqueue_script( 'jsf-financial-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'js-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array(jquery), '2', true );

	wp_enqueue_script( 'jsf-financial-custom', get_template_directory_uri() . '/js/custom.js', array(),
		'2',	true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jsf_financial_scripts' );


// Load jsf finantial styles
function jsf_financial_styles()
{
    wp_register_style('css-flexslider', get_template_directory_uri() . '/css/flexslider.css', array(), '1.0', 'all');
    wp_enqueue_style('css-flexslider'); // Enqueue it!


    wp_register_style('jsf-financial-style', get_stylesheet_uri(), array(), '2.0', 'all');
    wp_enqueue_style('jsf-financial-style'); // Enqueue it!


}
add_action('wp_enqueue_scripts', 'jsf_financial_styles'); // Add Theme


/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type
function create_post_type_resource_center()
{
		register_taxonomy(
	    'resource_category',
	    array( 'resource-centers' ),  // this is the custom post type(s) I want to use this taxonomy for
	    array(
	        'hierarchical' => true,
	        'label' => 'Resource center Category',
	        'query_var' => true,
	        'rewrite' => true
	    )
	  );
		register_taxonomy(
	    'resource_tag',
	    'resource-centers',  // this is the custom post type(s) I want to use this taxonomy for
	    array(
	        'hierarchical' => false,
	        'label' => 'Resource center Tags',
	        'query_var' => true,
	        'rewrite' => true
	    )
	  );
    register_post_type('resource-centers', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => 'Resource center', // Rename these to suit
            'singular_name' => 'Resource center',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Resource center',
            'edit' => 'Edit',
            'edit_item' => 'Edit Resource center',
            'new_item' => 'New Resource center',
            'view' => 'View Resource center',
            'view_item' => 'View Resource center',
            'search_items' => 'Search Resource center',
            'not_found' => 'No Resource center found',
            'not_found_in_trash' => 'No Resource center found in Trash'
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author',
            'comments'
        ),
        'can_export' => true, // Allows export in Tools > Export
        'menu_icon'  => 'dashicons-groups',
        'taxonomies' => array(
            'resource_category',
            'resource_tag'
        ) // Add Category and Post Tags support
    ));
}
add_action('init', 'create_post_type_resource_center'); // Add our

function create_post_type_team()
{
		register_taxonomy(
	    'teams_positions',
	    'teams',  // this is the custom post type(s) I want to use this taxonomy for
	    array(
	        'hierarchical' => true,
	        'label' => 'Teams Positions',
	        'query_var' => true,
	        'rewrite' => true
	    )
	  );
    register_post_type('teams', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => 'Team members', // Rename these to suit
            'singular_name' => 'Team members',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Team member',
            'edit' => 'Edit',
            'edit_item' => 'Edit Team member',
            'new_item' => 'New Team member',
            'view' => 'View Team member',
            'view_item' => 'View Team member',
            'search_items' => 'Search Team members',
            'not_found' => 'No Team member found',
            'not_found_in_trash' => 'No Team member found in Trash'
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author',
            'comments'
        ),
        'can_export' => true, // Allows export in Tools > Export
        'menu_icon'  => 'dashicons-businessman'
    ));
}
add_action('init', 'create_post_type_team'); // Add our jsf_financial Custom Post Type

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function jsf_financial_wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'mid_size' => 0
    ));
}
function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyDHClkphmbIgWr9djXnKs8OVBGgOtyw9VE';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// Exclude category in single post
function exclude_post_categories($excl='', $spacer=' '){
   $categories = get_the_category($post->ID);
      if(!empty($categories)){
      	$exclude=$excl;
      	$exclude = explode(",", $exclude);
		$thecount = count(get_the_category()) - count($exclude);
      	foreach ($categories as $cat) {
      		$html = '';
      		if(!in_array($cat->cat_ID, $exclude)) {
				$html .= '<a href="' . get_category_link($cat->cat_ID) . '" ';
				$html .= 'title="' . $cat->cat_name . '">' . $cat->cat_name;
				if($thecount > 0){
					$html .= $spacer;
				}
				$html .= '</a>';
			$thecount--;
      		echo $html;
      		}
	      }
      }
}
// Exclude custom taxonomy category in single post
function get_modified_term_list( $id = 0, $taxonomy, $before = '', $sep = '', $after = '', $exclude = array() ) {
    $terms = get_the_terms( $id, $taxonomy );

    if ( is_wp_error( $terms ) )
        return $terms;

    if ( empty( $terms ) )
        return false;

    foreach ( $terms as $term ) {

        if(!in_array($term->term_id,$exclude)) {
            $link = get_term_link( $term, $taxonomy );
            if ( is_wp_error( $link ) )
                return $link;
            $term_links[] = '<a href="' . $link . '" rel="tag">' . $term->name . '</a>';
        }
    }

    if( !isset( $term_links ) )
        return false;

    return $before . join( $sep, $term_links ) . $after;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
if (function_exists('acf_add_options_sub_page')) {
	if( function_exists('acf_add_options_page') ) {
 		acf_add_options_page(); // necessary for v.5 :-/
	}
	acf_add_options_sub_page( 'standard' );
	acf_add_options_sub_page( 'Wordpress Options' );
	acf_add_options_sub_page( 'Settings' );
    }
