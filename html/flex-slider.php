<div class="flex-container">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <div class="flex-caption">
                    <p>Tailored advice for life's unpredictable challenges</p>
                    <button>Learn more</button>
                </div>
                <div class="flex-image">
                    <img src="http://localhost/jsf-financial/wp-content/uploads/2017/10/Depositphotos_13512336_original-wheat-crop.jpg" alt="some photo">
                </div>
            </li>
            <li>
                <div class="flex-caption">
                    <p>Tailored advice for life's unpredictable challenges</p>
                    <button>Learn more</button>
                </div>
                <div class="flex-image">
                    <img src="http://localhost/jsf-financial/wp-content/uploads/2017/10/Depositphotos_13512336_original-wheat-crop.jpg" alt="some photo">
                </div>
            </li>
            <li>
                <div class="flex-caption">
                    <p>Tailored advice for life's unpredictable challenges</p>
                    <button>Learn more</button>
                </div>
                <div class="flex-image">
                    <img src="http://localhost/jsf-financial/wp-content/uploads/2017/10/Depositphotos_13512336_original-wheat-crop.jpg" alt="some photo">
                </div>
            </li>
            <li>
                <div class="flex-caption">
                    <p>Tailored advice for life's unpredictable challenges</p>
                    <button>Learn more</button>
                </div>
                <div class="flex-image">
                    <img src="http://localhost/jsf-financial/wp-content/uploads/2017/10/Depositphotos_13512336_original-wheat-crop.jpg" alt="some photo">
                </div>
            </li>
        </ul>
    </div>
</div>