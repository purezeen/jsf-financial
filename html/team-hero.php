<section class="team-hero">
    <div class="team-hero-left">
        <h1>The JSF people</h1>
        <p>Based on fast-paced, sunny Los Angeles, the JSF Financial team has a passion for helping our
        clients achieve their financial goals. We focus on listening to what is important and provide
        innovative wealth management and planning solutions - now and for the future</p>
    </div>
    <div class="team-hero-right">
        <img src="http://via.placeholder.com/200x200" alt="">
    </div>
</section>
