<?php /* Template Name: Team Page */ ?>
<?php include 'header.php'?>
<?php include 'team-hero.php'?>

<section class="team-members">
    <div class="team-founders">
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
    </div>
    <div class="gray-box">
        <h1>Investment management</h1>
        <p>At JSF, the investment management process follows a multi step consultative approach designed to develop customized investment strategies that incorporate our client's personal needs and objectives.</p>
    </div>
    <div class="team-management">
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
    </div>
    <div class="gray-box">
        <h1>Business Operations</h1>
        <p>In creating a financial plan, JSF wealth managers focus on both short-term financial needs and long term goals, taking a global view of each client's financial life.</p>
    </div>
    <div class="team-bussines">
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
    </div>
    <div class="gray-box">
        <h1>Account Operations</h1>
        <p>In creating a financial plan, JSF wealth managers focus on both short-term financial needs and long term goals, taking a global view of each client's financial life.</p>
    </div>
    <div class="team-account">
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
        <div class="team-member">
            <img src="http://via.placeholder.com/130x130" alt="team member">
            <p>Founder &amp; managing memeber/</p>
            <p>Investment Advisor Representative</p>
        </div>
    </div>
</section>

<?php include "footer.php"; ?>