<?php /* Template Name: Resource Center Repository page */ ?>
<?php include 'header.php'?>

<section class="resource-content">
    <div class="hero">
        <div class="hero-caption">
            <h1>You want me to do what ?</h1>
            <p class="by">By <span class="author">Sam Sekine</span>on <span class="date">SEP 23</span> in <span
                        class="tag">Estate planning</span></p>
            <p class="caption-text">You trust a financial adviser to handle your money. But what about a missing child ?
            Or a marriage at the breaking point ? <br> In the course of their job, advisers hear all sorts of
            secrets from their clients.</p>
            <a href="#">Read more</a>
        </div>
    </div>

    <div class="top">
        <div class="posts">
            <h1 class="trending-title">What's trending</h1>
            <div class="images">
                <div class="img-l">
                    <img src="http://via.placeholder.com/360x250" alt="">
                    <p class="trending-date">23 <span>November</span></p>
                </div>
                <div class="img-r">
                    <h1>Los Angeles county in smallest short-term issue since '08</h1>
                    <p class="short-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales felis sed porta porttitor. Phasellus fermentum lorem magna, vel mattis metus convallis in. Aliquam molestie dui id dui egestas, quis tempor orci efficitur. Quisque tincidunt consequat justo a egestas. </p>
                    <a href="#">Read more</a>
                </div>
            </div>
            <div class="images">
                <div class="img-l">
                    <img src="http://via.placeholder.com/360x250" alt="">
                    <p class="trending-date">23 <span>November</span></p>
                </div>
                <div class="img-r">
                    <h1>It's not all about you</h1>
                    <p class="short-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales felis sed porta porttitor. Phasellus fermentum lorem magna, vel mattis metus convallis in. Aliquam molestie dui id dui egestas, quis tempor orci efficitur. Quisque tincidunt consequat justo a egestas. </p>
                    <a href="#">Read more</a>
                </div>
            </div>
            <div class="images">
                <div class="img-l">
                    <img src="http://via.placeholder.com/360x250" alt="">
                    <p class="trending-date">23 <span>November</span></p>
                </div>
                <div class="img-r">
                    <h1>When financial disaster hits home</h1>
                    <p class="short-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales felis sed porta porttitor. Phasellus fermentum lorem magna, vel mattis metus convallis in. Aliquam molestie dui id dui egestas, quis tempor orci efficitur. Quisque tincidunt consequat justo a egestas. </p>
                    <a href="#">Read more</a>
                </div>
            </div>
        </div>
        <aside>
            <div class="categories">
                <ul>Categories
                    <li>Charitable giving</li>
                    <li>Culture</li>
                    <li>Estate planning</li>
                    <li>Financial advisors</li>
                    <li>Investing</li>
                    <li>Other</li>
                    <li>Worth sharing</li>
                </ul>
            </div>
            <div class="archives">
                <ul>Archives
                    <li>March 2017</li>
                    <li>January 2017</li>
                    <li>December 2016</li>
                    <li>November 2016</li>
                    <li>September 2016</li>
                </ul>
            </div>
            <div class="tag-cloud">
                <h1>Tag cloud</h1>
                <ul>
                    <li>Financial</li>
                    <li>Investors</li>
                    <li>Capital</li>
                    <li>Planning</li>
                    <li>Real-estate</li>
                    <li>News</li>
                    <li>Import</li>
                    <li>United states</li>
                    <li>Stock</li>
                    <li>Personal advisors</li>
                    <li>Foreign markets</li>
                </ul>
            </div>
        </aside>
    </div>
    <div class="bottom">
        <section class="resource-center-highlights">
            <div class="title">
                <h1>Resource center highlights</h1>
                <a href="#">View all</a>
            </div>
            <div class="posts">
                <div class="center-highlight">
                    <img src="http://via.placeholder.com/360x240" alt="">
                    <p>JSF wealth managers overview</p>
                    <a href="#">Read more</a>
                </div>
                <div class="center-highlight">
                    <img src="http://via.placeholder.com/360x240" alt="">
                    <p>Focusing on both short-term financial needs and long term goals</p>
                    <a href="#">Read more</a>
                </div>
                <div class="center-highlight">
                    <img src="http://via.placeholder.com/360x240" alt="">
                    <p>Financial planners in Los Angeles</p>
                    <a href="#">Read more</a>
                </div>
            </div>
        </section>
    </div>

</section>

<?php include 'footer.php'?>

