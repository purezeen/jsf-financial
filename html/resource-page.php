<?php /* Template Name: Resource Center Page */ ?>
<?php include 'header.php'?>

<section class="resource-content">
    <div class="hero-res"></div>
    <div class="top">
        <div class="content">
            <h1>Los Angeles county in smallest short-term issue since '08</h1>
            <p class="authored-by">By <span>Sam Sekine</span> on sep 23 in <span>estate planning</span></p>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales felis sed
                porta porttitor.
                Phasellus fermentum lorem magna, vel mattis metus convallis in. Aliquam molestie dui id dui egestas, quis tempor orci efficitur. Quisque tincidunt consequat justo a egestas. Etiam consequat ex pharetra nibh dictum volutpat. Aliquam id augue nulla. Praesent lacus nulla, tempor sed felis ac, vehicula rutrum elit. Integer tincidunt tellus id nulla egestas maximus. Donec scelerisque bibendum augue pretium mollis. Fusce convallis quis quam vitae suscipit.
                Ut suscipit diam faucibus, efficitur odio nec, pulvinar lacus. Proin purus nisi, maximus et ante eu, rhoncus vehicula felis. Integer nec augue risus. Mauris sodales est nec lectus viverra, a consequat dolor elementum. Ut ac enim non libero mollis rhoncus vel et erat. Integer ornare blandit lobortis. Aenean vitae posuere mauris, ac vestibulum diam. Fusce faucibus lorem et sem mattis, in mollis erat semper. Sed eget augue eget odio pellentesque vehicula a a ex. Quisque condimentum eros nec eros tempor tristique. Ut eget ornare urna. Praesent condimentum felis id urna tincidunt ullamcorper. Praesent ac magna ultrices, congue enim quis, rhoncus est. Maecenas maximus arcu ullamcorper, interdum purus et, hendrerit orci. Etiam lacinia risus nec viverra laoreet.
                Nulla leo elit, bibendum nec erat ac, volutpat mattis quam. Proin placerat, lacus sed semper hendrerit, massa justo varius augue, ut sagittis felis dolor scelerisque nunc. Donec tristique consectetur imperdiet. Nam vulputate augue in nunc aliquam, sit amet euismod enim consequat. Pellentesque imperdiet condimentum varius. Aenean id justo in neque mollis vestibulum. Donec tempor, dui ac venenatis sodales, ante felis lacinia ante, at efficitur elit sapien viverra eros. Fusce tempus eros non libero euismod, quis mattis quam posuere. Curabitur id luctus eros. Praesent euismod porta risus, vitae semper enim luctus ac. Nullam nec risus a ante fringilla placerat vel quis diam. In ut placerat dolor. Vestibulum purus sem, commodo non fringilla sit amet, efficitur sit amet nibh. Mauris sit amet fringilla mi, sit amet congue odio. Integer sed varius ex, id tincidunt nisl.
                Nam molestie ex id massa hendrerit gravida. Duis eu fringilla nisl, vel elementum augue. Praesent tortor lorem, malesuada sit amet posuere at, egestas vitae enim. Duis lacinia lorem mattis, maximus neque sit amet, feugiat lacus. Donec finibus a lectus eu tincidunt. Phasellus sed neque posuere, egestas nulla nec, facilisis ipsum. Duis vitae nulla enim. Vestibulum a ornare diam. Cras auctor lectus dolor, id rhoncus dolor cursus et. In ullamcorper pretium rhoncus. Cras lobortis risus ex, eget dapibus est egestas vel. Sed congue, lacus tempor finibus tempus, elit dui volutpat mauris, at mollis orci elit non urna. Suspendisse et enim quam. Quisque ultrices metus ac consectetur luctus. Etiam id massa dignissim, tristique lorem et, posuere justo. Phasellus feugiat scelerisque dolor ac finibus.
            </p>
            <img src="http://via.placeholder.com/770x513" alt="resource image">
            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
            <p class="description">
                Nam molestie ex id massa hendrerit gravida. Duis eu fringilla nisl, vel elementum augue. Praesent
                tortor lorem, malesuada sit amet posuere at, egestas vitae enim. Duis lacinia lorem mattis, maximus neque sit amet, feugiat lacus. Donec finibus a lectus eu tincidunt. Phasellus sed neque posuere, egestas nulla nec, facilisis ipsum. Duis vitae nulla enim. Vestibulum a ornare diam. Cras auctor lectus dolor, id rhoncus dolor cursus et. In ullamcorper pretium rhoncus. Cras lobortis risus ex, eget dapibus est egestas vel. Sed congue, lacus tempor finibus tempus, elit dui volutpat mauris, at mollis orci elit non urna. Suspendisse et enim quam. Quisque ultrices metus ac consectetur luctus. Etiam id massa dignissim, tristique lorem et, posuere justo. Phasellus feugiat scelerisque dolor ac finibus.
            </p>
            <div class="pager">
                <a class="prev" href="#">PREV</a>
                <a class="next" href="#">NEXT</a>
            </div>

        </div>
        <aside>
            <div class="about-author">
                <div class="short-info">
                    <p class="post-date">23 <span>November</span></p>
                    <img src="http://via.placeholder.com/50x50" alt="">
                </div>
                <div class="author-social">
                    <img src="http://via.placeholder.com/50x50" alt="facebook">
                    <img src="http://via.placeholder.com/50x50" alt="twitter">
                    <img src="http://via.placeholder.com/50x50" alt="linkedin">
                    <img src="http://via.placeholder.com/50x50" alt="pinterest">
                    <img src="http://via.placeholder.com/50x50" alt="google plus">
                </div>
                <div class="author-info">
                    <p class="about">About the author</p>
                    <p class="name">Sam Sekine</p>
                    <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales felis sed
                        porta porttitor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales felis sed
                        porta porttitor.</p>
                </div>
            </div>
            <div class="categories">
                <ul>Categories
                    <li>Charitable giving</li>
                    <li>Culture</li>
                    <li>Estate planning</li>
                    <li>Financial advisors</li>
                    <li>Investing</li>
                    <li>Other</li>
                    <li>Worth sharing</li>
                </ul>
            </div>
            <div class="archives">
                <ul>Archives
                    <li>March 2017</li>
                    <li>January 2017</li>
                    <li>December 2016</li>
                    <li>November 2016</li>
                    <li>September 2016</li>
                </ul>
            </div>
            <div class="tag-cloud">
                <h1>Tag cloud</h1>
                <ul>
                    <li>Financial</li>
                    <li>Investors</li>
                    <li>Capital</li>
                    <li>Planning</li>
                    <li>Real-estate</li>
                    <li>News</li>
                    <li>Import</li>
                    <li>United states</li>
                    <li>Stock</li>
                    <li>Personal advisors</li>
                    <li>Foreign markets</li>
                </ul>
            </div>
        </aside>
    </div>
    <div class="bottom">
        <div class="title">
            <h1>What's trending</h1>
            <a href="#">View all</a>
        </div>
        <div class="trending">
            <div class="images">
                <img src="http://via.placeholder.com/360x250" alt="">
                <div class="description">
                    <p class="image-title">JSF wealth managers overview</p>
                    <p class="trending-date">23 <span>November</span></p>
                </div>
                <a href="#">Read more</a>
            </div>
            <div class="images">
                <img src="http://via.placeholder.com/360x250" alt="">
                <div class="description">
                    <p class="image-title">Focusing on both short-term financial needs and long-term goals</p>
                    <p class="trending-date">23 <span>November</span></p>
                </div>
                <a href="#">Read more</a>
            </div>
            <div class="images">
                <img src="http://via.placeholder.com/360x250" alt="">
                <div class="description">
                    <p class="image-title">Financial planners in Los Angeles</p>
                    <p class="trending-date">23 <span>November</span></p>
                </div>
                <a href="#">Read more</a>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'?>

